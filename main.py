#!/usr/bin/env python3
# from __future__ import print_function

from flask import Flask, redirect, send_from_directory
from flask_cors import CORS, cross_origin

app = Flask('api')
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
@cross_origin()
def hello_world():
    return redirect('https://rstreebank.ru')


@app.route('/names')
@cross_origin()
def names():
    return send_from_directory('assets/', 'names.json')

@app.route('/markers')
@cross_origin()
def markers():
    return send_from_directory('assets/', 'markers.json')

@app.route('/file/<string:id>')
@cross_origin()
def text_all_id(id):
    return send_from_directory('assets/files/', id+'.json')


@app.route('/html/<string:id>')
@cross_origin()
def html_id(id):
    return send_from_directory('assets/htmls/', id+'.html')


@app.route('/rs3/<string:id>')
@cross_origin()
def rs3_id(id):
    return send_from_directory('assets/rs3s/', id+'.rs3', as_attachment=True)


@app.route('/txt/<string:id>')
@cross_origin()
def txt_id(id):
    return send_from_directory('assets/txts/', id+'.txt', as_attachment=True)


@app.route('/assets/<path:path>')
@cross_origin()
def assets(path):
    return send_from_directory('assets/', path)


@app.route('/doc/<string:file>')
@cross_origin()
def docs(file):
    return send_from_directory('assets/docs/', file)


@app.route('/archive/<string:file>')
@cross_origin()
def archives(file):
    return send_from_directory('assets/archives/', file)
